﻿var R = (t0  = 0, t1  = 1, t2  = 2, t3  = 3,
    t4  = 4, t5  = 5, t6  = 6, t7  = 7,
    t8  = 8, t9  = 9, t10 = 10, t11 = 11,
    t12 = 12, t13 = 13, t14 = 14, t15 = 15,
    t16 = 16, t17 = 17, t18 = 18, t19 = 19,
    t20 = 20, t21 = 21, t22 = 22, t23 = 23,
    t24 = 24, t25 = 25, t26 = 26, t27 = 27,
    t28 = 28, t29 = 29, t30 = 30, t31 = 31,
    t32 = 32, t33 = 33, t34 = 34, t35 = 35,
    t36 = 36, t37 = 37, t38 = 38, t39 = 39,
    t40 = 40, t41 = 41, t42 = 42, t43 = 43,
    t44 = 44, t45 = 45, t46 = 46, t47 = 47,
    t48 = 48, t49 = 49, t50 = 50, t51 = 51,
    t52 = 52, t53 = 53, t54 = 54, t55 = 55,
    t56 = 56, t57 = 57, t58 = 58, t59 = 59,
    t60 = 60, t61 = 61, t62 = 62, t63 = 63,
    fn: any = x => x,
    v1  = [], v2  = [], v3  = [], v4  = [],
    v5  = [], v6  = [], v7  = [], v8  = [],
    v9  = [], v10 = [], v11 = [], v12 = [],
    v13 = [], v14 = [], v15 = [], v16 = [],
    v17 = [], v18 = [], v19 = [], v20 = [],
    v21 = [], v22 = [], v23 = [], v24 = [],
    v25 = [], v26 = [], v27 = [], v28 = [],
    v29 = [], v30 = [], v31 = [], v32 = [],
    v33 = [], v34 = [], v35 = [], v36 = [],
    v37 = [], v38 = [], v39 = [], v40 = [],
    v41 = [], v42 = [], v43 = [], v44 = [],
    v45 = [], v46 = [], v47 = [], v48 = [],
    v49 = [], v50 = [], v51 = [], v52 = [],
    v53 = [], v54 = [], v55 = [], v56 = [],
    v57 = [], v58 = [], v59 = [], v60 = [],
    v61 = [], v62 = [], v63 = [], v64 = []
    ) =>
    t0 != t1 && t2 != t3
    && t4 != t5 && t6 != t7
    && t8 != t9 && t10 != t11
    && t12 != t13 && t14 != t15
    && t16 != t17 && t18 != t19
    && t20 != t21 && t22 != t23
    && t24 != t25 && t26 != t27
    && t28 != t29 && t30 != t31
    && t32 != t33 && t34 != t35
    && t36 != t37 && t38 != t39
    && t40 != t41 && t42 != t43
    && t44 != t45 && t46 != t47
    && t48 != t49 && t50 != t51
    && t52 != t53 && t54 != t55
    && t56 != t57 && t58 != t59
    && t60 != t61 && t62 != t63
    && v1.push(t0)
    && v2.unshift(t1)
    && v3.push(t2)
    && v4.unshift(t3)
    && v5.push(t4)
    && v6.unshift(t5)
    && v7.push(t6)
    && v8.unshift(t7)
    && v9.push(t8)
    && v10.unshift(t9)
    && v11.push(t10)
    && v12.unshift(t11)
    && v13.push(t12)
    && v14.unshift(t13)
    && v15.push(t14)
    && v16.unshift(t15)
    && v17.push(t16)
    && v18.unshift(t17)
    && v19.push(t18)
    && v20.unshift(t19)
    && v21.push(t20)
    && v22.unshift(t21)
    && v23.push(t22)
    && v24.unshift(t23)
    && v25.push(t24)
    && v26.unshift(t25)
    && v27.push(t26)
    && v28.unshift(t27)
    && v29.push(t28)
    && v30.unshift(t29)
    && v31.push(t30)
    && v32.unshift(t31)
    && v33.push(t32)
    && v34.unshift(t33)
    && v35.push(t34)
    && v36.unshift(t35)
    && v37.push(t36)
    && v38.unshift(t37)
    && v39.push(t38)
    && v40.unshift(t39)
    && v41.push(t40)
    && v42.unshift(t41)
    && v43.push(t42)
    && v44.unshift(t43)
    && v45.push(t44)
    && v46.unshift(t45)
    && v47.push(t46)
    && v48.unshift(t47)
    && v49.push(t48)
    && v50.unshift(t49)
    && v51.push(t50)
    && v52.unshift(t51)
    && v53.push(t52)
    && v54.unshift(t53)
    && v55.push(t54)
    && v56.unshift(t55)
    && v57.push(t56)
    && v58.unshift(t57)
    && v59.push(t58)
    && v60.unshift(t59)
    && v61.push(t60)
    && v62.unshift(t61)
    && v63.push(t62)
    && v64.unshift(t63)
    &&
    R(t0 + 1, t1 - 1, t2 + 1, t3 - 1,
        t4 + 1, t5 - 1, t6 + 1, t7 - 1,
        t8 + 1, t9 - 1, t10 + 1, t11 - 1,
        t12 + 1, t13 - 1, t14 + 1, t15 - 1,
        t16 + 1, t17 - 1, t18 + 1, t19 - 1,
        t20 + 1, t21 - 1, t22 + 1, t23 - 1,
        t24 + 1, t25 - 1, t26 + 1, t27 - 1,
        t28 + 1, t29 - 1, t30 + 1, t31 - 1,
        t32 + 1, t33 - 1, t34 + 1, t35 - 1,
        t36 + 1, t37 - 1, t38 + 1, t39 - 1,
        t40 + 1, t41 - 1, t42 + 1, t43 - 1,
        t44 + 1, t45 - 1, t46 + 1, t47 - 1,
        t48 + 1, t49 - 1, t50 + 1, t51 - 1,
        t52 + 1, t53 - 1, t54 + 1, t55 - 1,
        t56 + 1, t57 - 1, t58 + 1, t59 - 1,
        t60 + 1, t61 - 1, t62 + 1, t63 - 1,
        fn,
        v1, v2, v3, v4, v5, v6, v7, v8,
        v9, v10, v11, v12, v13, v14, v15, v16,
        v17, v18, v19, v20, v21, v22, v23, v24,
        v25, v26, v27, v28, v29, v30, v31, v32,
        v33, v34, v35, v36, v37, v38, v39, v40,
        v41, v42, v43, v44, v45, v46, v47, v48,
        v49, v50, v51, v52, v53, v54, v55, v56,
        v57, v58, v59, v60, v61, v62, v63, v64)
    || v2.unshift(t1)
    ? v1.concat(v2).concat(v3).concat(v4)
        .concat(v5).concat(v6).concat(v7)
        .concat(v8).concat(v9).concat(v10)
        .concat(v11).concat(v12).concat(v13)
        .concat(v14).concat(v15).concat(v16)
        .concat(v17).concat(v18).concat(v19)
        .concat(v20).concat(v21).concat(v22)
        .concat(v23).concat(v24).concat(v25)
        .concat(v26).concat(v27).concat(v28)
        .concat(v29).concat(v30).concat(v31)
        .concat(v32).concat(v33).concat(v34)
        .concat(v35).concat(v36).concat(v37)
        .concat(v38).concat(v39).concat(v40)
        .concat(v41).concat(v42).concat(v43)
        .concat(v44).concat(v45).concat(v46)
        .concat(v47).concat(v48).concat(v49)
        .concat(v50).concat(v51).concat(v52)
        .concat(v53).concat(v54).concat(v55)
        .concat(v56).concat(v57).concat(v58)
        .concat(v59).concat(v60).concat(v61)
        .concat(v62).concat(v63).concat(v64).map(fn)
    : v64

var c = R(
    0,
    1000 * 1.0, 1001 * 1.0, 2000 * 1.0, 2001 * 1.0, 3000 * 1.0, 3001 * 1.0, 4000 * 1.0, 4001 * 1.0,
    5000 * 1.0, 5001 * 1.0, 6000 * 1.0, 6001 * 1.0, 7000 * 1.0, 7001 * 1.0, 8000 * 1.0, 8001 * 1.0,
    9000 * 1.0, 9001 * 1.0, 10000 * 1.0, 10001 * 1.0, 11000 * 1.0, 11001 * 1.0, 12000 * 1.0, 12001 * 1.0,
    13000 * 1.0, 13001 * 1.0, 14000 * 1.0, 14001 * 1.0, 15000 * 1.0, 15001 * 1.0, 16000 * 1.0, 16001 * 1.0,
    17000 * 1.0, 17001 * 1.0, 18000 * 1.0, 18001 * 1.0, 19000 * 1.0, 19001 * 1.0, 20000 * 1.0, 20001 * 1.0,
    21000 * 1.0, 21001 * 1.0, 22000 * 1.0, 22001 * 1.0, 23000 * 1.0, 23001 * 1.0, 24000 * 1.0, 24001 * 1.0,
    25000 * 1.0, 25001 * 1.0, 26000 * 1.0, 26001 * 1.0, 27000 * 1.0, 27001 * 1.0, 28000 * 1.0, 28001 * 1.0,
    29000 * 1.0, 29001 * 1.0, 30000 * 1.0, 30001 * 1.0, 31000 * 1.0, 31001 * 1.0,
    33000 * 1.0,
    x=> x * x
    );
console.log(c);